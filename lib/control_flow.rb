# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  new_str = ""
  for i in 0...str.length
    if str[i] != str[i].downcase
      new_str << str[i]
    end
  end
  return new_str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length.even?
    return str[(str.length - 1) / 2] + str[str.length / 2]
  else
    return str[str.length / 2]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  count = 0
  for i in 0...str.length
    if VOWELS.include?(str[i])
      count += 1
    end
  end
  return count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  if num <= 1
    return 1
  else
    return num * factorial(num - 1)
  end
end

# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  join_string = ""
  arr.each do |string|
    if string == arr.last
      join_string << string
    else
      join_string << string + separator
    end
  end
  return join_string
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  weird_string = ""
  for i in 0...str.length
    if i.odd?
      weird_string << str[i].upcase
    else
      weird_string << str[i].downcase
    end
  end
  return weird_string
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  split_string = str.split
  for i in 0...split_string.length
    word = split_string[i]
    if word.length >= 5
      split_string[i] = word.reverse
    end
  end
  return split_string.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  i = 1
  fizz_array = []
  while i <= n
    if i % 3 == 0 && i % 5 != 0
      fizz_array << "fizz"
    elsif i % 5 == 0 && i % 3 != 0
      fizz_array << "buzz"
    elsif i % 15 == 0
      fizz_array << "fizzbuzz"
    else
      fizz_array << i
    end
    i += 1
  end
  return fizz_array
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  return arr.reverse!
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  i = 2
  return false if num < 2
  while i < num
    return false if num % i == 0
    i += 1
  end
  return true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factors_arr = []
  i = 1
  while i <= num
    factors_arr << i if num % i == 0
    i += 1
  end
  return factors_arr
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  prime_factors_arr = []
  i = 2
  while i <= num
    prime_factors_arr << i if num % i == 0 && prime?(i) == true
    i += 1
  end
  return prime_factors_arr
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  return prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)

def oddball(arr)
  odd_arr = []
  even_arr = []
  arr.each do |num|
    odd_arr << num if num.odd?
    even_arr << num if num.even?
  end

  return odd_arr.join.to_i if odd_arr.length < even_arr.length
  return even_arr.join.to_i if even_arr.length < odd_arr.length
end
